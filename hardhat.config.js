require("@nomicfoundation/hardhat-toolbox");
require("@nomiclabs/hardhat-ethers");
require("@openzeppelin/hardhat-upgrades");
require("@nomiclabs/hardhat-etherscan");

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  defaultNetwork: "mumbai",
  networks: {
    hardhat: {
    },
    mumbai: {
      chainId: 80001,
      url: "https://polygon-mumbai.g.alchemy.com/v2/PB24npcn2oasy7lYkUIU4w2X1Olb-QFU",
      accounts: [`0x${'bf8a34e10b37531f5fdcd3e3ccea4cbc57fba7f59179b7a086cb54cfa70b9438'}`],
    },
    polygon:{
      chainId:137,
      url:"https://polygon.llamarpc.com",
      accounts:[`0x${'c21bbca8af5f3786ee4fd96610942a3f908a43ec72f66da7ae9c40aee86445e4'}`]
    },
    ethereum:{
      chainId:1,
      url:"https://eth.llamarpc.com",
      accounts:[`0x${'c21bbca8af5f3786ee4fd96610942a3f908a43ec72f66da7ae9c40aee86445e4'}`]
    },
    sepolia: {
      chainId: 11155111,
      url: "https://eth-sepolia.g.alchemy.com/v2/fTYJNkhybenpIFXHijQAE5Ed52mXSp_i",
      accounts: [`0x${'bf8a34e10b37531f5fdcd3e3ccea4cbc57fba7f59179b7a086cb54cfa70b9438'}`]
    }
  },
  solidity: {
    version: "0.8.9",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  },
  paths: {
    artifacts: "./pages/artifacts"
  },
  etherscan: {
    apiKey: "FTDWQQADSKJRTW3ZXZGR5P729GDVN7QRAR",
  }
};
