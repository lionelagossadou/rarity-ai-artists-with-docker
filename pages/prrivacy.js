import React from 'react'
import Main from '../templates/Main';
import { Meta } from '../layouts/Meta';

const About = () => {
  return (
    <Main>
      <Meta
        title="Privacy Policy - Rarity AI Artists"
        description="About Rarity AI Artists"
      />
      <div className="px-8 md:px-20 min-h-[80vh] lg:px-32 xl:px-40 2xl:px-52 pt-16 text-gray-600">
        <h1 className="text-4xl xl:text-5xl font-bold text-left text-black">Privacy Policy</h1>
        <div className="mt-12 flex flex-col text-2xl gap-6">
          <p>Welcome to our platform, a community haven for AI art artists who want to sell their creations as NFT, find engagements for their works or simply share them.
            Our goal is to create a supportive community for AI artists, where they can share their work, connect with other artists, and sell their creations directly to their fans.
          </p>
          <p>
            Our vision is to become the leading support collection for AI art artists around the world. We want to create a supportive community for AI art artists, where they can share their work, connect with other artists and sell their creations directly to their fans.
            We believe that AI generated art is the future of art and we are committed to helping AI art artists succeed.
          </p>
          <p>
            We are here to help you and look forward to helping you succeed and make a name for yourself in the world of NFTs and a place in the big circle of these artists.
          </p>
        </div>
      </div>
    </Main>
  )
}

export default About;
