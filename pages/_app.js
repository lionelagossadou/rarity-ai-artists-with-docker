import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import ConnectWallet from '../components/connectWallet';
import ApproveTransaction from '../components/ApproveTransaction';
import TransactionInProgress from '../components/TransactionInProgress';
import '../styles/globals.css';

import { persistor, store } from '../store/store';

function MyApp({ Component, pageProps: { session, ...pageProps } }) {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
          <div><Component {...pageProps} /></div>
          <ConnectWallet />
      </PersistGate>
    </Provider>
  )
}

export default MyApp
