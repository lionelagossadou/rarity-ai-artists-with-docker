const hre = require("hardhat");

async function main() {
  const NAME = "Starmint"; // Remplacer par le nom de votre collection
  const CONTRACT_URI = "https://gateway.pinata.cloud/ipfs/QmfZgCjhHVyjqjC3kj1SsCBfLiUK5gVjwftkeXvzCwboMP"; // Remplacer par l'URI de votre contrat
  const DEFAULT_ROYALTIES = 500; // 5%
  const ROYALTIES_RECEIVER_ADDRESS = "0xF02BBa5a85eE0fD193368acF7A814D296EE8DA5e";
  const StarmintETH = await hre.ethers.getContractFactory("StarmintEth");

  const starmintETH = await hre.upgrades.deployProxy(StarmintETH, [
    NAME,
    DEFAULT_ROYALTIES, // Remplacer par le pourcentage des royalties en basis points (1/10000)
    ROYALTIES_RECEIVER_ADDRESS,
    CONTRACT_URI], {
    initializer: "initialize",
  });

  await starmintETH.deployed();

  console.log("Starmint ETH contract deployed to:", starmintETH.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });