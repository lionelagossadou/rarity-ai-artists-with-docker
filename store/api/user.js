import { REQUEST, USER_ERROR } from "../reducers/userReducer";

export const request = (query) => async (dispatch) => {
    try {
        dispatch(REQUEST(query));
    } catch (error) {
        dispatch(USER_ERROR('Unable to process'));
    }
};